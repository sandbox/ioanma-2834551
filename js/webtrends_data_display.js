/**
 * @file
 * Handles Tab transitions etc for the Webtrends Data Display UI.
 */
(function ($) {
    Drupal.behaviors.webtrends_data_display = {
        attach: function (context) {

            // Time range button highlights.
            $(".block-webtrends-data-display .content a.use-ajax").click(function () {
                $(".block-webtrends-data-display .content a.active-webtrends-button").removeClass("active-webtrends-button");
                $(this).addClass("active-webtrends-button");
            });

            // Data siblings show/hide & device tab highlights.
            $(".webtrends-list:eq(0)").show();
            $(".webtrends-data-heading:eq(0)").addClass("active-device-tab");
            $(".webtrends-data-heading").click(function () {
                var index = $( ".webtrends-data-heading" ).index(this);
                $(".webtrends-list").hide();
                $(".webtrends-list:eq("+index+")").show();

                $(".webtrends-data-heading.active-device-tab").removeClass("active-device-tab");
                $(this).addClass("active-device-tab");
            });

        }
    };
})(jQuery);
