<?php

/**
 * @file
 * Webtrends Analytics credentials & settings page.
 */

/**
 * Webtrends data admin settings form.
 */
function _webtrends_data_display_admin_settings_form($form, &$form_state) {
  $credentials = _webtrends_data_display_credentials();

  $form['webtrends_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Webtrends API username'),
    '#description' => t('Webtrends username (sometimes it includes a "your_company\" prefix).'),
    '#default_value' => !empty($credentials['webtrends_username']) ? $credentials['webtrends_username'] : NULL,
    '#required' => TRUE,
  );

  $form['webtrends_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Webtrends API password'),
    '#description' => t('Webtrends password.'),
    '#default_value' => !empty($credentials['webtrends_pass']) ? $credentials['webtrends_pass'] : NULL,
    '#required' => TRUE,
  );

  $form['webtrends_profile_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Webtrends API profile id'),
    '#description' => t('Webtrends profile id.'),
    '#default_value' => !empty($credentials['webtrends_profile_id']) ? $credentials['webtrends_profile_id'] : NULL,
    '#required' => TRUE,
  );
  
  $form['webtrends_report_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Webtrends API report id'),
    '#description' => t('Webtrends report id.'),
    '#default_value' => !empty($credentials['webtrends_report_id']) ? $credentials['webtrends_report_id'] : NULL,
    '#required' => TRUE,
  );
  
  $form['webtrends_site_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Webtrends site domain'),
    '#description' => t('Webtrends sites domain (e.g. \'your-domain-name.com\').'),
    '#default_value' => !empty($credentials['webtrends_site_domain']) ? $credentials['webtrends_site_domain'] : NULL,
    '#required' => TRUE,
  );

  $caching_options = array(
    'one_hour' => t('Cache webtrends data for 1 hour'),
    'two_hours' => t('Cache webtrends data for 2 hours'),
    'three_hours' => t('Cache webtrends data for 3 hours'),
  );

  $webtrends_duration_var = variable_get('webtrends_caching_duration');
  $default_caching_duration = !empty($webtrends_duration_var) ? $webtrends_duration_var : 'three_hours';

  $form['caching_duration'] = array(
    '#type' => 'select',
    '#title' => t('Choose caching duration'),
    '#description' => t('Select one of the available caching options for webtrends data.'),
    '#options' => $caching_options,
    '#default_value' => $default_caching_duration,
    '#required' => TRUE,
  );
  
  // Get default site email.
  $site_email = variable_get('site_mail', '');
  
  $form['support_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Support email'),
    '#description' => t('Support email address for webrends related issues.'),
    '#default_value' => variable_get('webtrends_support_email', $site_email),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Settings'),
    '#submit' => array('_webtrends_data_display_admin_settings_form_submit'),
  );

  return $form;
}

/**
 * Form submission callback for _webtrends_data_display_admin_settings_form().
 */
function _webtrends_data_display_admin_settings_form_submit($form, &$form_state) {
  variable_set('webtrends_caching_duration', $form_state['values']['caching_duration']);
  
  $site_email = variable_get('site_mail', '');
  if (!empty($form_state['values']['support_email'])) {
    variable_set('webtrends_support_email', $site_email);
  }
  else {
    variable_set('webtrends_support_email', $form_state['values']['support_email']);
  }

  // Get any existing API account id for the country.
  $query = db_select('webtrends_data_display');
  $query->fields('webtrends_data_display', array('wddid'));
  $result = $query->execute()->fetchField();

  $wddid = ($result) ? $result : NULL;

  db_merge('webtrends_data_display')
    ->key(array('wddid' => $wddid))
    ->fields(array(
      'webtrends_username' => $form_state['values']['webtrends_username'],
      'webtrends_pass' => $form_state['values']['webtrends_password'],
      'webtrends_profile_id' => $form_state['values']['webtrends_profile_id'],
      'webtrends_report_id' => $form_state['values']['webtrends_report_id'],
    ))
    ->execute();

  drupal_set_message(t('Webtrends Data Display settings saved.'), 'system');
}
