<?php

/**
 * @file
 * Template file for webtrends data display.
 */
?>
<div class="webtrends-data-display-wrapper">
  <?php if (!empty($webtrends_parameters['days_of_data']) && $webtrends_parameters['days_of_data'] < 7) : ?>
    <p class="webtrends-detail">
      <span><?php print t('Detail'); ?></span><?php print t('Please be aware that the results shown are for') . ' '; ?>
      <strong><?php print format_plural($webtrends_parameters['days_of_data'], '1 day', '@count days'); ?></strong>
    </p>
  <?php endif; ?>

  <?php if (!empty($webtrends_parameters['metrics'])) : ?>
    <section class="webtrends-tabs">
      <?php foreach ($webtrends_parameters['metrics'] as $device => $metrics) { ?>
        <div>
          <h2 class="webtrends-data-heading <?php print strtolower($device); ?>"><?php print $device; ?></h2>
        </div>
      <?php } ?>
    </section>
    <div class="webtrends-device-data">
      <?php foreach ($webtrends_parameters['metrics'] as $device => $metrics) { ?>
        <ul class="webtrends-list">
          <?php foreach ($metrics as $key => $values) { ?>
            <?php if (is_array($values)) :?>
              <li>
                <h3 class="webtrends-data-subheading <?php print strtolower(str_replace(' ', '-', $key)); ?>"><?php print $key; ?></h3>
                <div class="webtrends-value"><?php print $values['absolute_value']; ?></div>
                <?php if ($device != 'Total') : ?>
                  <div class="webtrends-percent-value"><?php print $values['percentage']; ?></div>
                <?php endif; ?>
              </li>
            <?php endif; ?>
          <?php } ?>
          <?php if (!empty($webtrends_parameters['metrics'][$device]['all_zeros'])) : ?>
            <div class="webtrends-no-data">
              <p>
                <span><?php print t('No data'); ?></span>
                <?php print t('There is no data for this device yet.'); ?>
              </p>
            </div>
          <?php endif; ?>
        </ul>
      <?php } ?>
    </div>
  <?php endif; ?>

  <div class="webtrends-info">
    <span><?php print t('Info'); ?></span>
    <p><?php print t('For a detailed breakdown of your results please visit <a target="_blank" href="@webtrends-analytics-portal">Webtrends</a> or if you require access contact <a href="@webtrends-support-email">the web analytics team</a>.', array(
      '@webtrends-analytics-portal' => WEBTRENDS_DD_ANALYTICS_URL,
      '@webtrends-support-email' => variable_get('webtrends_support_email', $site_email),
      )); ?>
    </p>
  </div>
</div>
