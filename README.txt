Webtrends Data Display
-------------
This module provides an easy way to view Webtrends analytics within your Drupal website using the Data Extraction API (https://help.webtrends.com/en/dxapi). A webtrends account and profile ID are needed for this module to work.

Features
--------
- Configuration form for storing Webtrends credentials, Webtrends profile ID, caching duration and support email.
- Webtrends analytics displays on every node / revision edit pages.
- Last day and last 7 days reports.
- Metrics available per device type (Mobile, Tablet, Desktop) plus totals.
- 6 metrics displayed: Page views, visits, page views per visit, average view time, page views per visit and bounce rate.
- 3 caching duration options (1, 2 or 3 hours).
- Editable support email in case your users need help with setting up a Webtrends account (defaults to site mail).

INSTALLATION
------------
Install as you would normally install a contributed Drupal module. See: https://drupal.org/documentation/install/modules-themes/modules-7 for further information.

CONFIGURATION
------------
After installing the module, configure user permissions in Administration » People » Permissions: View Webtrends Data Display information.
Configuration for this module can be found in Administration >> Configuration >> Web Services >> Webtrends Data Display. No data will be returned until all required fields are saved.

The module is using the Webtrends Data Extraction API (http://help.webtrends.com/en/dxapi) and constructs a query url using a number of parameters documented here: http://help.webtrends.com/en/dxapi/parameters_dx.html. The query url is expected to be in the following form: https://ws.webtrends.com/v3/Reporting/profiles/xxxxx/reports/yyyyyyyyyyy/?totals=all&start_period=current_day-7&end_period=current_day-1&period_type=indv&query=Page%20like%20%22%22your_viewed_node_path&sortby=Visits&format=json.

- The profile and report ids can be found in your Webtrends account. Custom profiles and reports can also be generated.
- The totals parameter specifies whether, or how, totals for measures are returned. In this case, all measures are requested.
- The start_period parameter is used to specify the beginning day of a date range (e.g. current_day-7) and the end_period to specify the ending day of the date range (e.g. current_day-1). On the node edit page, users can choose between last day and last 7 days of data and the ajax callback provides the selected time range to the query url dynamically.
- The period_type parameter is used to specify how to return the report data. This is set to indv which returns every period in the period range individually (“multi-dimensional report”).
- The query parameter is used to filter returned data. The module is using the “Page” dimension which is given the node url being edited: query=Page%20like%20%22%22full_node_url. The full node url is composed by the site domain (e.g. “your-domain.com”), the current language (if more than one languages are enabled this would be the language of the node currently being edited, e.g. “en”) and finally the node alias (e.g. “category/node-title”). The full node url is expected in this form: “your-domain.com/en/category/node-title” and - at least for the time being - different node url formats are not supported. Practically speaking, if your website urls follow the structure mentioned you don’t need to change anything in your languages / multisite configuration.
- The sortby parameter is used to sort report data by a measure, in this case Visits is used.
- The format parameter is set to json.
- The suppress_error_codes parameter is set to true as we don’t normally want to be notified of error conditions for a request on a live environment.

Sponsor
-------
The module was initially developed for the British Council (http://www.britishcouncil.org).

Authors
-------
Originally developed by

- Marios Ioannidis (https://www.drupal.org/u/weborion)
- Andrea Dos Santos
- Lucja Wszola (https://www.drupal.org/u/lucja-wszola)
